/**
 * These functions are namespaced by jQuery, it helps make sure the names don't conflict with other peoples code since we work with opensource code.
 * !If the global functions are related to maps, charts, or tables. Put them somewhere else
 */

(function($){

	$.fn.sortObject = function(property, AscOrDec = 'asc') { 
		var sortOrder = 1;
		if (AscOrDec == "dec") sortOrder = -1;
		return function (a,b) {
			var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
			return result * sortOrder;
		}
	}
	var numberOfIndicatorsToPrint = 0;
	$.fn.addIndicatorToReport = function(indicator, type) { 
		$('div#report-link').show(200);
		numberOfIndicatorsToPrint++;
		if(type == 'Table'){
			reportIndicators[indicator+type] = activeIndicators['#data-table-'+indicator+type];
		} else {
			reportIndicators[indicator+type] = activeIndicators[indicator+type];
		}
		$('div#report-number').text(numberOfIndicatorsToPrint);
	}
	$.fn.removeIndicatorFromReport = function(indicator, type) { 
		numberOfIndicatorsToPrint--;
		delete reportIndicators[indicator+type];
		if (numberOfIndicatorsToPrint == 0){
			$('div#report-link').hide(200);
		} else {
			$('div#report-number').text(numberOfIndicatorsToPrint);
		}
	}
	$.fn.goToIndicatorReport = function() { 
		console.log(reportIndicators);
		$.jStorage.set("country-report", reportIndicators );
		var value = $.jStorage.get("country-report");
		window.location.href = "/reporting";
		
	}
	$.fn.exists = function () {
    	return this.length !== 0;
	}

	$.fn.getWindowHeight = function () {
		var height = $(window).height();// - $('#admin-menu-wrapper').outerHeight(true) + $('#messages').outerHeight(true);
		var adminHeight = 0;
		var menuHeight = 0
		if ($('#block-mainnavigation')[0]){
			menuHeight = $("#block-mainnavigation").height();
		}
		if ($('#toolbar-administration')[0]){
			adminHeight =  79 ; //the admin toolbar is exactly 79px tall
		} 
		height = height - (adminHeight + menuHeight);
		heightPaddingFix = adminHeight + menuHeight ; 
		$('#container-page').css('top', heightPaddingFix);
		return height; 
	}

	$.fn.createAccordion = function (id, title = '', icon = null, parent = null) {
		var parentText = ''
		var iconText = '';
		if (parent){
			parentText = " data-parent='#" + parent + "'";
		}
		if (icon){
			iconText = " <i class='" + icon + "'></i>";
		}
		var accordion = "<button class='biopama-collapse my-2 btn btn-outline-success w-100 d-flex justify-content-between' type='button' data-bs-toggle='collapse' data-bs-target='#collapse" + id + "' aria-expanded='false' aria-controls='collapse" + id + "'>" +
		"<div class='collapse-title'> " + iconText + " " + title + " </div><div class='card-collapsed'><i class='fas fa-chevron-down'></i></div><div class='card-expanded'><i class='fas fa-chevron-up'></i></div>"+
		"</button>" +
		"<div class='collapse wrapper-card text-bg-dark' id='collapse" + id + "' " + parentText + ">"+
			"<div class='mb-3 border-0'>" +
				"<div class='card-body'>" +
					"<div class='card-text p-3'>" +
					"</div>" +
				"</div>" +
			"</div>" +
		"</div>";
		return accordion; 
	}

	$.fn.biopamaReplaceTokens = function (string, token = null) {
		//if it's an object it MUST be from the CT module for tracking active variables, so we assume it's fine and these properties must be present
		//if the properties are undefined, well... you are doing something wrong.
		if (typeof token === 'object'){
			string = string.replace("[NUM]", token.NUM)
			.replace("[ISO2]", token.ISO2)
			.replace("[ISO3]", token.ISO3)
			.replace("[COUNTRYNAME]", token.countryName)
			.replace("[WDPAID]", token.WDPAID)
			.replace("[REGION]", token.regionID)
			.replace("NUM", token.NUM) 			//for backwards compatability (we added brackets) - to be removed at somepoint - START
			.replace("ISO2", token.ISO2) 		//
			.replace("ISO3", token.ISO3)		//
			.replace("WDPAID", token.WDPAID)	//
			.replace("REGION", token.regionID);	// END
		} else if (typeof token === 'string' || typeof token === 'number' ){
			string = string.replace("[TOKEN]", token) //if the token is a simple string.
		} else {
			console.log("a token replacement was attempted, but something went wrong");
		}
		return string; 
	}

	$.fn.getKeys = function (obj) {
		var keys = [];
		for(var key in obj){
			keys.push(key);
		}
		return keys;
	}

	$.fn.insertBiopamaLoader = function (selector, position) {
		//$().insertBiopamaLoader(selector); 
		var loader = "<div class='mini-loader-wrapper'><div id='mini-loader'></div></div>";
		if(position == 'before'){
			$(loader).insertBefore(selector);
			return;
		} else if(position == 'after'){
			$(loader).insertAfter(selector);
			return;
		} else {
			$(selector).append(loader);
		}
	}
	$.fn.removeBiopamaLoader = function (selector) {
		//$().removeBiopamaLoader(selector); 
		//removes any and all loaders from within the target selector
		$(selector).find(".mini-loader-wrapper").remove();
	}

	$.fn.createRegionalSelect = function () {

		var regionalSelect = "<select id='region-selector' data-placeholder='Choose a region...' class='biopama-region-selector chosen-select form-control text-bg-dark'>"+
		"<option value='' disabled selected>Select a region</option>";
		for (var region in biopamaGlobal.regions) {
			var name = biopamaGlobal.regions[region].name;
			var id = biopamaGlobal.regions[region].path;
			if (name !== "ACP"){
				regionalSelect += "<option value='" +id+ "'>"+name+"</option>";
			}
		}

		regionalSelect += "</select>";
		return regionalSelect; 
	}

	$.fn.createNationalSelect = function () {
		//var regionsArray = ;
		//console.log(biopamaGlobal.regions)
		var nationalSelect = "<select id='country-selector' data-placeholder='Choose a country...' class='biopama-country-selector chosen-select form-control text-bg-dark'>"+
		"<option value='' disabled selected>Select a country</option>";
		
		for (var region in biopamaGlobal.regions) {
			var name = biopamaGlobal.regions[region].name;

			if(name == "ACP Caribbean"){
				nationalSelect += "<optgroup label='Caribbean'></optgroup>"+
				"<option value='AG'>Antigua and Barbuda</option>"+
				"<option value='BS'>Bahamas</option>"+
				"<option value='BB'>Barbados</option>"+
				"<option value='BZ'>Belize</option>"+
				"<option value='CU'>Cuba</option>"+
				"<option value='DM'>Dominica</option>"+
				"<option value='DO'>Dominican Republic</option>"+
				"<option value='GD'>Grenada</option>"+
				"<option value='GY'>Guyana</option>"+
				"<option value='HT'>Haiti</option>"+
				"<option value='JM'>Jamaica</option>"+
				"<option value='KN'>Saint Kitts and Nevis</option>"+
				"<option value='LC'>Saint Lucia</option>"+
				"<option value='VC'>Saint Vincent and the Grenadines</option>"+
				"<option value='SR'>Suriname</option>"+
				"<option value='TT'>Trinidad and Tobago</option>";
			}
			if(name == "Western Africa"){
				nationalSelect += "<optgroup label='Western Africa'></optgroup>"+
				"<option value='BJ'>Benin</option>"+
				"<option value='BF'>Burkina Faso</option>"+
				"<option value='CV'>Cabo Verde</option>"+
				"<option value='CI'>Côte d'Ivoire</option>"+
				"<option value='GM'>Gambia</option>"+
				"<option value='GH'>Ghana</option>"+
				"<option value='GN'>Guinea</option>"+
				"<option value='GW'>Guinea-Bissau</option>"+
				"<option value='LR'>Liberia</option>"+
				"<option value='ML'>Mali</option>"+
				"<option value='MR'>Mauritania</option>"+
				"<option value='NE'>Niger</option>"+
				"<option value='NG'>Nigeria</option>"+
				"<option value='SN'>Senegal</option>"+
				"<option value='SL'>Sierra Leone</option>"+
				"<option value='TG'>Togo</option>";
			}
			if(name == "Central Africa"){
				nationalSelect += "<optgroup label='Central Africa'></optgroup>"+
				"<option value='BI'>Burundi</option>"+
				"<option value='CM'>Cameroon</option>"+
				"<option value='CF'>Central African Republic</option>"+
				"<option value='TD'>Chad</option>"+
				"<option value='CG'>Congo</option>"+
				"<option value='CD'>Congo (Democratic Republic of the)</option>"+
				"<option value='GQ'>Equatorial Guinea</option>"+
				"<option value='GA'>Gabon</option>"+
				"<option value='ST'>Sao Tome and Principe</option>";
			}
			if(name == "Eastern Africa"){
				nationalSelect += "<optgroup label='Eastern Africa'></optgroup>"+
				"<option value='DJ'>Djibouti</option>"+
				"<option value='ER'>Eritrea</option>"+
				"<option value='ET'>Ethiopia</option>"+
				"<option value='KE'>Kenya</option>"+
				"<option value='RW'>Rwanda</option>"+
				"<option value='SO'>Somalia</option>"+
				"<option value='SS'>South Sudan</option>"+
				"<option value='SD'>Sudan</option>"+
				"<option value='TZ'>Tanzania, United Republic of</option>"+
				"<option value='UG'>Uganda</option>";
			}
			if(name == "Southern Africa"){
				nationalSelect += "<optgroup label='Southern Africa'></optgroup>"+
				"<option value='AO'>Angola</option>"+
				"<option value='BW'>Botswana</option>"+
				"<option value='KM'>Comoros</option>"+
				"<option value='SZ'>Eswatini</option>"+
				"<option value='LS'>Lesotho</option>"+
				"<option value='MG'>Madagascar</option>"+
				"<option value='MW'>Malawi</option>"+
				"<option value='MU'>Mauritius</option>"+
				"<option value='MZ'>Mozambique</option>"+
				"<option value='NA'>Namibia</option>"+
				"<option value='SC'>Seychelles</option>"+
				"<option value='ZA'>South Africa</option>"+
				"<option value='ZM'>Zambia</option>"+
				"<option value='ZW'>Zimbabwe</option>";
			}
			if(name == "ACP Pacific"){
				nationalSelect += "<optgroup label='Pacific'></optgroup>"+
				"<option value='CK'>Cook Islands</option>"+
				"<option value='FJ'>Fiji</option>"+
				"<option value='KI'>Kiribati</option>"+
				"<option value='MH'>Marshall Islands</option>"+
				"<option value='FM'>Micronesia (Federated States of)</option>"+
				"<option value='NR'>Nauru</option>"+
				"<option value='NU'>Niue</option>"+
				"<option value='PW'>Palau</option>"+
				"<option value='PG'>Papua New Guinea</option>"+
				"<option value='WS'>Samoa</option>"+
				"<option value='SB'>Solomon Islands</option>"+
				"<option value='TL'>Timor-Leste</option>"+
				"<option value='TO'>Tonga</option>"+
				"<option value='TV'>Tuvalu</option>"+
				"<option value='VU'>Vanuatu</option>";
			}
		}
		
		nationalSelect += "</select>";
		return nationalSelect; 
	}

})(jQuery);

