(function($){
	$.fn.createDataTable = function(tableElementOrSelector, tableData, report = false){

		let $table = $().selectorCheck(tableElementOrSelector); 
		
		if ($.fn.DataTable.isDataTable($table)){ //to see if a datatable is here. Kill it if it is found
			var table = $table.DataTable();
			table.destroy();
			$table.empty();
		}

		if (!tableData.drawCallback){
			tableData.drawCallback = false;
		}

		let defaultButtons = [
			{
				extend: 'print',
				title: tableData.title,
				messageBottom: tableData.attribution,
			},{
				extend: 'excel',
				title: tableData.title,
				messageBottom: tableData.attribution,
			},{
				extend: 'copy',
				title: tableData.title, 
				messageBottom: tableData.attribution,
			},{
				extend: 'csv',
				title: tableData.title,
				messageBottom: tableData.attribution,
			},
		];

		if (typeof activeIndicators !== 'undefined'  ){
			activeIndicators[tableElementOrSelector+'Table'] = tableData;
			activeIndicators[tableElementOrSelector+'Table'].type = 'table';
		}

		if (report){
			tableData.isComplex = false;
			defaultButtons = [];
			tableData.buttons = [];
		}

		let dt_table = $table.DataTable({
			data: tableData.data,
			columns : tableData.columns,
			columnDefs: tableData.columnDefs,
			order: tableData.defaultSort,
			dom: tableData.dom || 'Bfrtip',
			paging: tableData.pagination || tableData.isComplex,
			searching: tableData.isComplex,
			ordering:  tableData.isComplex,
			info: tableData.isComplex,
			responsive: tableData.responsive || false,
			drawCallback: tableData.drawCallback,
			pageLength: tableData.pageLength || 10,
			buttons: tableData.buttons || defaultButtons,
			createdRow: tableData.createdRow,
			rowCallback: tableData.rowCallback
		});

		if ($(tableElementOrSelector).length >= 1){
			$().removeBiopamaLoader(tableElementOrSelector);
		}

		return dt_table;
	}

	$.fn.updateCellColors = function(cardTwigName, colors){
		colors.forEach(function (item, index) {
			$('.' + cardTwigName + "-cell-color-" + index).css("color", item);
		});
	}
})(jQuery);
