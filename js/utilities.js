/**
 * These functions are namespaced by jQuery, it helps make sure the names don't conflict with other peoples code since we work with opensource code.
 * 
 */

 (function($){

	$.fn.selectorCheck = function (thisSelector) {
		let $selectorElement;
		switch(typeof thisSelector){
			case 'string':
				if (thisSelector.indexOf("#") == -1){
					$selectorElement = $("#"+thisSelector); // For backward-compatibility.
				} else {
					$selectorElement = $(thisSelector);
				}
				if($selectorElement.length == 0){
					$selectorElement = $(thisSelector);
					if($selectorElement.length == 0){
						throw new Error("Not a valid selector.");
					}
				}
				break;
			case 'object':
				if(thisSelector instanceof jQuery){
					$selectorElement = thisSelector;
				}
				else{
					$selectorElement = $(thisSelector);
				}
				break;
			default:
				throw new Error("Not a valid selector.");
		}
		return $selectorElement; 
	}

})(jQuery);

