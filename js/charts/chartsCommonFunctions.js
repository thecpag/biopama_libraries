(function($){
    $.fn.getChartDomElement = function(elementOrSelector){
        let element = elementOrSelector;
        if(typeof elementOrSelector == 'string'){
            element = $(elementOrSelector).get(0);
        }
        return element;
    }
})(jQuery);
