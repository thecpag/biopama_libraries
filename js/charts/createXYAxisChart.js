(function($){
	$.fn.createXYAxisChart = function(chartSelector, chartData, report = false){ 
/**
 * 
 * Code that is needed for the global dataset module
 * todo - remove it?
 * 
 */
		if (report){
			thisTitle = {};
			thisTooltip = {};
			thisToolbox = {};
		}
		if (typeof activeIndicators !== 'undefined'  ){
			activeIndicators[chartSelector+'Chart'] = chartData;
			activeIndicators[chartSelector+'Chart'].type = 'chart';
			activeIndicators[chartSelector+'Chart'].chartType = 'XYAxis';
		}
/**
 * 
 * end code needed for global dataset module
 * 
 */

 		let $chart = $().selectorCheck(chartSelector).get(0); 
	
		var chartTitle = null;
		var thisToolbox = null;
		var thisTooltip = null;
		var chartDataZoom = null;

		if(chartData.title){
			chartTitle = {
				text: chartData.title,
				show: true,
			};
		}

		if(!chartData.toolbox){
			thisToolbox = {
				show: true,
				right: 20,
				top: -8,
				feature: {
					saveAsImage: {
						title: 'Save',
					}
				}
			};
		}

		if(!chartData.tooltip){
			thisTooltip = {
				trigger: 'axis'
			};
			//thisTooltip = biopamaGlobal.chart.tooltip;
		}

		if(chartData.dataZoom){
			chartDataZoom = [
				{
					type: 'slider',
					xAxisIndex: [0],
					filterMode: 'filter'
				}
			];
		}

		if (report){
			chartTitle = {};
			thisTooltip = {};
			thisToolbox = {};
		}
		
		chartData.xAxis['nameLocation'] = "middle";
		chartData.xAxis['nameGap'] = 30;

		chartData.yAxis['nameLocation'] = "middle";
		chartData.yAxis['nameRotate'] = 90;
		chartData.yAxis['nameGap'] = 60;
		
		var indicatorChart = echarts.init($.fn.getChartDomElement($chart));
		var option = { 
			title: chartTitle, 
			legend: chartData.legend,
			xAxis: chartData.xAxis,
			tooltip: chartData.tooltip || thisTooltip,
			toolbox: chartData.toolbox || thisToolbox,
			yAxis: chartData.yAxis,
			series: chartData.series,
			dataZoom: chartDataZoom,
			animationDuration: 600,
		};
		
		if (chartData.colors){
			chartData.color = chartData.colors;
		} 
		if (chartData.color) {
			var colors = chartData.color;
			if (typeof colors  === 'string'){
				if (colors !== "ignore"){
					option.color = biopamaGlobal.chart.colors[chartData.color];
				} else {
					option.color = [];
				}
			} else if (Array.isArray(colors)){
				option.color = chartData.color;
			}
		} else {
			option.color = biopamaGlobal.chart.colors.default;
		}

		option.xAxis['axisLine'] = { lineStyle: { color: biopamaGlobal.chart.colors.text}};
		option.xAxis['axisLabel'] = { lineStyle: { color: biopamaGlobal.chart.colors.text}};
		option.yAxis['axisLine'] = { lineStyle: { color: biopamaGlobal.chart.colors.text}};
		option.yAxis['axisLabel'] = { lineStyle: { color: biopamaGlobal.chart.colors.text}};
		
		if ($(chartSelector).length >= 1){
			$().removeBiopamaLoader(chartSelector);
		}

		indicatorChart.setOption(option);

		return indicatorChart;
	}
})(jQuery);
