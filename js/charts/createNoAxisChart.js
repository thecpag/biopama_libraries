(function($){
	$.fn.createNoAxisChart = function(chartSelector, chartData, report = false){

		let $chart = $().selectorCheck(chartSelector).get(0); 

		var chartTitle = null;
		var chartToolbox = null;
		var chartTooltip = null;
		var chartTitleTextAlign = null;
		
		if (report){
			chartTitle = {};
			chartTooltip = {};
			chartToolbox = {};
		}
		if (typeof activeIndicators !== 'undefined'  ){
			activeIndicators[chartSelector+'Chart'] = chartData;
			activeIndicators[chartSelector+'Chart'].type = 'chart';
			activeIndicators[chartSelector+'Chart'].chartType = 'NoAxis';
		}

		if(chartData.titleAlign){
			chartTitleTextAlign = chartData.titleAlign;
		}

		if(chartData.title){
			chartTitle = {
				text: chartData.title,
				show: true,
				left: chartTitleTextAlign
			};
		}

		if(!chartData.toolbox){
			chartToolbox = biopamaGlobal.chart.toolbox;
		}

		if(!chartData.tooltip){
			chartTooltip = {
				trigger: 'item'
			};
			//chartTooltip = biopamaGlobal.chart.tooltip;
		}


		//pie charts are VERY similar to line and bar charts, but all the config data is in the series, so we need a separate function 
		var indicatorChart = echarts.init($.fn.getChartDomElement($chart));
		var option = {
			title: chartTitle,
			toolbox: chartToolbox, 
			tooltip: chartData.tooltip || chartTooltip, 
  			legend: chartData.legend,
			series: chartData.series,
			animationDuration: 600
		};


		if(chartData.singleVal){
			option.series[0]['labelLine'] = {show: false};
			option.series[0]['selectedMode'] = 'single';
			option.series[0]['avoidLabelOverlap'] = false;
			option.series[0]['label'] = {
				position: 'center',
				fontSize: '20',
				//fontWeight: 'bold',
				color: biopamaGlobal.chart.colors.text,
			};
			option.series[0].data[0]['label'] = {show: true};
			option.series[0].data[1]['emphasis'] = { disabled: true};
			option.series[0].data[1]['label'] = {show: false};
		} else {
			option['textStyle'] = {color: biopamaGlobal.chart.colors.text}
		}

		if (chartData.colors){
			var colors = chartData.colors;
			if (typeof colors  === 'string'){
				option.color = biopamaGlobal.chart.colors[chartData.colors];
			} else if (Array.isArray(colors)){
				option.color = chartData.colors;
			}
		} else {
			option.color = biopamaGlobal.chart.colors.default;
		}

		if (chartData.dataset){
			option.dataset = chartData.dataset;
		} 
		
		if ($(chartSelector).length >= 1){
			$().removeBiopamaLoader(chartSelector);
		}

		indicatorChart.setOption(option);

		return indicatorChart;
	}
})(jQuery);
