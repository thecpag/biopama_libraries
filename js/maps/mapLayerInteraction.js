var closePopupIcon;

(function($){

    $.fn.addMapLayerInteraction = function(map, popupOptions = {}){
        /**
         * These options are to allow the URLs in the popup to be controlled depending on what module is invoking the function
         * If a function should be run on like click set the Link value to '#' and create an onClick event where you need it.
            popupOptions = {
                regionLink: '',
                countryLink: '',
                paLink: '',
            }
         * 
         */
        
        var theseOptions = {
            region: {
                url: popupOptions.regionLink || '#', 
            },
            country: {
                url: popupOptions.countryLink || '#', 
            },
            pa: {
                url: popupOptions.paLink || '#', 
            }, 
        }
        
        $.fn.attachMapEventHandler(
            map,
            'load',
            function(){
                _addMapLayerInteraction(map, theseOptions);
            }
        );
    }
    _addMapLayerInteraction = function(map, theseOptions){

        map.on('click', getFeatureInfo);
	
        var popUpContents = {};
    
        function getFeatureInfo(e){
            popUpContents = {
                region: '',
                regionID: '',
                country: '',
                countryCode: '',
                country3Code: '',
                PAs: [],
                WDPAIDs: [],
                paIUCNCats: [],
                paSize: [],
            };
            new Promise(function(resolve, reject) {
            regionCheck(e)
            }).then(countryCheck(e))
            .then(paCheck(e))
            .then(makePopUp(e));
        }

        function makePopUp(e){

           
            if (popUpContents.regionID.length){
                var areaCheck = "areaInactive";
                if (selSettings.regionName == popUpContents.region)areaCheck = "areaActive";
                var popup = popUpSection(areaCheck, popUpContents.region, popUpContents.regionID, "region");
                var regionPopup = new mapboxgl.Popup({anchor:'right', className: 'biopamaPopup', offset: 40, closeButton: false})
                    .setLngLat(e.lngLat)
                    .setHTML(popup)
                    .addTo(map);
            } 
            
            if (popUpContents.countryCode.length){
                var areaCheck = "areaInactive";
                if (selSettings.ISO2 == popUpContents.countryCode)areaCheck = "areaActive";
                var popup = popUpSection(areaCheck, popUpContents.country, popUpContents.countryCode, "country");
                var countryPopup = new mapboxgl.Popup({anchor:'bottom', className: 'biopamaPopup', offset: 40, closeButton: false})
                    .setLngLat(e.lngLat)
                    .setHTML(popup)
                    .addTo(map);
            } 
    
            var numberOfPas = "singlePA";
            var paPopupContent = '';
            if (popUpContents.PAs.length){
                if (popUpContents.PAs.length == 1){
                    checkPA(popUpContents.WDPAIDs[0], 0)
                } else {
                    numberOfPas = "multiPAs"
                    for (var key in popUpContents.PAs) {
                        checkPA(popUpContents.WDPAIDs[key], key)
                    }
                }
                var paPopup = new mapboxgl.Popup({anchor:'left', className: 'biopamaPopup '+numberOfPas, offset: 40, closeButton: false})
                    .setLngLat(e.lngLat)
                    .setHTML(paPopupContent)
                    .addTo(map);
            }
            function checkPA(paID, paKey){
                var areaCheck = "areaInactive";
                if (paID == selSettings.WDPAID)areaCheck = "areaActive";
                paPopupContent = paPopupContent + popUpSection(areaCheck, popUpContents.PAs[paKey], popUpContents.WDPAIDs[paKey], "pa");
            }

            if (closePopupIcon !== undefined){
                closePopupIcon.remove();
            }

            var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]:not(.tooltipified)'))
			var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
				$(tooltipTriggerEl).addClass("tooltipified");
				return new bootstrap.Tooltip(tooltipTriggerEl)
			})

            if ((popUpContents.regionID.length) || (popUpContents.countryCode.length) || (popUpContents.PAs.length)){
                // create a HTML element for the centre
                var el = document.createElement('div');
                el.className = 'mapCloseMarker';
                el.innerHTML = '<i class="fas fa-times"></i>';
                
                closePopupIcon = new mapboxgl.Marker(el).setLngLat(e.lngLat).addTo(map);	

                $( "div.mapCloseMarker" ).click(function(e) {
                    e.stopPropagation();
                    closeMapCards();
                });
            } 
 
            $( "div.region div.cardSelect" ).click(function(e) {
                selSettings.regionName = popUpContents.region;
                selSettings.regionID = popUpContents.regionID;
                updateRegionFromPopup(selSettings.regionID);
                closeMapCards();
            });
            $( "div.country div.cardSelect" ).click(function(e) {
                selSettings.ISO2 = popUpContents.countryCode;
                var region = getMapLayerInfo(e.point, "regionMask");
                if (typeof region !== 'undefined') {
                    selSettings.regionID = popUpContents.regionID;
                }
                updateCountryFromPopup();
                closeMapCards();
            });
            $( "div.pa div.cardSelect" ).click(function(e) {
                selSettings.WDPAID = $(this).attr('id');
                selSettings.paName = $(this).closest(".card").find(".area-subheading").text().trim();
                selSettings.ISO3 = popUpContents.country3Code;
                selSettings.ISO2 = popUpContents.countryCode;
                updatePaFromPopup();
                closeMapCards();
            });
            $( "div.card.region" ).hover(function(e) {
                map.setFilter('regionHover', ['==', 'Group', popUpContents.region]);		
                map.setLayoutProperty("regionHover", 'visibility', 'visible');	
            });
            $( "div.card.country" ).hover(function(e) {
                map.setFilter('countryHover', ['==', 'iso3', popUpContents.country3Code]);		
                map.setLayoutProperty("countryHover", 'visibility', 'visible');	
            });
            $( "div.card.pa" ).hover(function(e) {
                var wdpaID = parseInt($(this).attr('id'), 10);
                map.setFilter('wdpaAcpHover', ['==', 'wdpaid', wdpaID]);		
                map.setLayoutProperty("wdpaAcpHover", 'visibility', 'visible');	
            });
            function closeMapCards(){
                $('.tooltipified').tooltip('dispose');
                if (regionPopup !== undefined)regionPopup.remove();
                if (countryPopup !== undefined)countryPopup.remove();
                if (paPopup !== undefined)paPopup.remove();
                closePopupIcon.remove();
            }
            function popUpSection(activeStatus, cardTitle, spatialID, spatialScope){
                var cardArea = spatialScope;
                var cardURL = theseOptions[spatialScope].url;
                if (spatialScope == 'pa'){
                    cardArea = 'Protected Areas';
                }
                if (theseOptions[spatialScope].url !== '#'){
                    cardURL = cardURL + spatialID;
                }
                var tempCard = '<div class="card mapPopupCard text-black mb-3 '+ activeStatus +' ' + spatialScope + '" id="'+spatialID+'">'+
                    '<div class="card-header card-header-'+ spatialScope +'">'+ cardArea + '</div>'+
                    '<div class="card-body cardSelect" id="'+spatialID+'" >'+
                        '<div class="row">'+
                            '<div class="col-12" data-bs-toggle="tooltip" data-bs-placement="top" title="Select this '+cardArea+'">'+
                                '<h6 class="area-subheading">' + cardTitle + '</h6>'+
                            '</div>'+
                            '<div class="col-12 area-icons">'+
                                '<a href="' + cardURL + '" class="cardIcon" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Goto this '+cardArea+' page"><i class="fas fa-arrow-right"></i></a>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</div>';
                return tempCard;
            }
        }
        
        function regionCheck(e){
            var region = getMapLayerInfo(e.point, "regionMask");
            if (typeof region !== 'undefined') {
                popUpContents.region = region[0].properties.Group;
                switch(popUpContents.region) {
                    case "Eastern Africa":
                        popUpContents.regionID = "eastern_africa";
                        break;
                    case "Central Africa":
                        popUpContents.regionID = "central_africa";
                        break;
                    case "Western Africa":
                        popUpContents.regionID = "western_africa";
                        break;
                    case "Southern Africa":
                        popUpContents.regionID = "southern_africa";
                        break;
                    default:
                        popUpContents.regionID = popUpContents.region;
                        break;
                    }
            } 
            return;
        }

        function countryCheck(e){
            var country = getMapLayerInfo(e.point, "countryMask");
            if (typeof country !== 'undefined') {
                popUpContents.country = country[0].properties.name_iso31;
                popUpContents.countryCode = country[0].properties.iso2; //For the Country Page
                popUpContents.country3Code = country[0].properties.iso3; //For PA feature matching
            }
            return;
        }
    
        function paCheck(e){
            var PAs = getMapLayerInfo(e.point, "wdpaAcpMask");
            if (typeof PAs !== 'undefined') {
                for (var key in PAs) {
                    popUpContents.PAs.push(PAs[key].properties.name);
                    popUpContents.WDPAIDs.push(PAs[key].properties.wdpaid);
                    popUpContents.paIUCNCats.push(PAs[key].properties.iucn_cat);
                    popUpContents.paSize.push(PAs[key].properties.gis_area);
                    if (PAs[key].properties.wdpaid == selSettings.WDPAID){
                        
                    }
                }		
            }
            return;
        }

    }

    //returns the first country object
    function getMapLayerInfo(point, mapLayer){
        var thisLayer = thisMap.getLayer(mapLayer);
        
        if(typeof thisLayer !== 'undefined') {
            var feature = thisMap.queryRenderedFeatures(point, {
                layers:[mapLayer],
            });
            //as long as we have something in the feature query 
            if (typeof feature[0] !== 'undefined'){
                return feature;
            }
        } else {
            return;
        }
    }
})(jQuery);