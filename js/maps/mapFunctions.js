
var countryZoomAjax;
var paZoomAjax;

(function($){

    $.fn.mapZoomToRegion = function zoomToRegion(map, region){
        stopAjaxZoomRequests();
        var layerZoomBox = checkMapLayerZoomPadding();
        switch(region){
          case "central_africa":
          case "Central Africa":
          case "CAF":
            selSettings.regionID = "central_africa";
            selSettings.regionName = "Central Africa";
            map.fitBounds([[1.8683898449,24.8886363352], [36.1896789074,-16.0012446593]], layerZoomBox);
            break;
          case "eastern_africa":
          case "Eastern Africa":
          case "EAF":
            selSettings.regionID = "eastern_africa";
            selSettings.regionName = "Eastern Africa";
            map.fitBounds([[20.3034484386,26.6692628716], [54.6247375011,-14.0916051203]], layerZoomBox);
            break;
          case "western_africa":
          case "Western Africa":
          case "WAF":
            selSettings.regionID = "western_africa";
            selSettings.regionName = "Western Africa";
            map.fitBounds([[-28.1462585926,31.1678846111], [20.4572570324,-0.7446243056]], layerZoomBox);
            break;
          case "southern_africa":
          case "Southern Africa":
          case "SAF":
            selSettings.regionID = "southern_africa";
            selSettings.regionName = "Southern Africa";
            map.fitBounds([[6.5265929699,-5.3073515284], [61.0187804699,-47.2924889494]], layerZoomBox);
            break;
          case "Pacific":
          case "PAC":
            selSettings.regionID = "Pacific";
            selSettings.regionName = "Pacific";
            map.fitBounds([[123.75,-24.846565], [216.914063,18.312811]], layerZoomBox);
            break;
          case "Caribbean":
          case "CAR":
            selSettings.regionID = "Caribbean";
            selSettings.regionName = "Caribbean";
            map.fitBounds([[-93.691406,-1.581830], [-51.240234,28.844674]], layerZoomBox);
            break;
          default:	//the country charts are the default for now....
            return;
        }
    }

    /**
     * 
     * @param {*} countryCode //the ISO2 country code
     * 
     */
    $.fn.mapZoomToCountryIso2 = function zoomToCountryIso2(map, countryCode){
      stopAjaxZoomRequests();
      var layerZoomBox = checkMapLayerZoomPadding();
      var zoomUrl = biopamaRestServices.biopamaCountryBounds;
      zoomUrl = zoomUrl.replace("[TOKEN]", countryCode);
      if(countryCode === 'FJ'){
        map.fitBounds([[166.61,-26.39], [192.01,-9.62]], layerZoomBox);
      } else if (countryCode === 'TV'){
        map.fitBounds([[168.58,-13.60], [191.48,-3.88]], layerZoomBox);
      } else if (countryCode === 'KI'){
        map.fitBounds([[-201.57,-15.70], [-136.18,10.31]], layerZoomBox);
      } else {
        countryZoomAjax = $.ajax({
          url: zoomUrl,
          dataType: 'json',
          success: function(d) {
            map.fitBounds([[d.features[0].properties.bbox[0],d.features[0].properties.bbox[1]], [d.features[0].properties.bbox[2],d.features[0].properties.bbox[3]]], layerZoomBox);
          },
          error: function() {
              console.log("Something is wrong with the REST servce for country bounds")
          }
        });
      }
    }
    /**
     * 
     * @param {*} countryCode //the ISO3 country code
     * 
     */
    $.fn.mapZoomToCountry = function zoomToCountry(map, countryCode){
      stopAjaxZoomRequests();
      var layerZoomBox = checkMapLayerZoomPadding();
      let serviceUrl = biopamaRestServices.countryISO3bounds + (Array.isArray(countryCode)? countryCode.join('|') : countryCode);
      
      countryZoomAjax = $.ajax({
        url: serviceUrl,
        dataType: 'json',
        success: function(responseData){
          try{
            let bounds = $.parseJSON(responseData[0].api_bbox_for_countries_dateline_safe, layerZoomBox);
            map.fitBounds(bounds);
          }
          catch(ex){
            console.log("zoom to country error:");
            console.log(ex);
          }
        },
        error: function() {
            console.log("Something is wrong with the REST servce for country bounds");
        }
      });
    }

    $.fn.setMapBounds = function(map, bounds){
      map.fitBounds(bounds);
    }

    $.fn.mapZoomToPA = function zoomToPA(map, wdpaid){
      stopAjaxZoomRequests();
      var layerZoomBox = checkMapLayerZoomPadding();
      var zoomUrl = $().biopamaReplaceTokens(biopamaRestServices.biopamaPaBounds, wdpaid);
      paZoomAjax = $.ajax({
        url: zoomUrl,
        dataType: 'json',
        success: function(d) {
          //console.log(d.features[0].properties)
          map.fitBounds([[d.features[0].properties.bbox[0],d.features[0].properties.bbox[1]], [d.features[0].properties.bbox[2],d.features[0].properties.bbox[3]]], layerZoomBox);
          
          selSettings.paName = d.features[0].properties.name;
          if (d.features[0].properties.ISO2 != selSettings.ISO2){
              selSettings.ISO2 = d.features[0].properties.ISO2;
          }
        },
        error: function() {
          console.log("Something is wrong with the REST servce for PA bounds");
        }
      });
    }

    function stopAjaxZoomRequests(e){
      if (paZoomAjax != null){ 
        paZoomAjax.abort();
        paZoomAjax = null;
      }
      if (countryZoomAjax != null){ 
        countryZoomAjax.abort();
        countryZoomAjax = null;
      }
    }

    $.fn.attachMapEventHandler = function(map, eventType, handler){
      map.on(
        eventType,
        handler
      );
    }

    $.fn.getSelectedFeatures = function(map, selectPoint, selectedLayers){
      return map.queryRenderedFeatures(
        selectPoint,
        {
          layers: selectedLayers
        }
      );
    }

    $.fn.createMapPopup = function(map, lngLatCoords, popupContent){
      new mapboxgl.Popup().setLngLat(lngLatCoords).setHTML(popupContent).addTo(map);
    }

    $.fn.setMapFilter = function(map, layerId, filter, options){
      map.setFilter(layerId, filter, options);
    }

    $.fn.addCardMapLayer = function(cardID, layerArgs = '', layerID = null){
      if (activeLayers.hasOwnProperty(cardID)){
        return; //just checking if the layer is already in the map. If it is we skip the function.
      }
      if ($(this).hasClass("custom-layer-args")) return; //some layer buttons need to be managed elsewhere. 
      
      var cardTheme = $("#collapse"+cardID).closest( ".wrapper-card" ).prev().text().trim();
      var cardLayers = drupalSettings.dataCards[cardTheme].data[cardID].layers;
      var cardLayerData;
      
      //if the card has buttons, find the selected/active button and add the layer it is referencing
      //otherwise just add the first layer
      //$("#collapse"+cardID).find('button.layer-button.active').attr("data-layer-id");
  
      if (layerID){
        cardLayerData = cardLayers.find(obj => { return obj.id === layerID }); //assign the layer that got passed in via id
      } else {
        //if the card has buttons, find the selected/active button and add the layer it is referencing
        //otherwise just add the first layer
        if ($("#collapse"+cardID).find('button.layer-button.active').exists()){
          layerID = $("#collapse"+cardID).find('button.layer-button.active').attr("data-layer-id");
          cardLayerData = cardLayers.find(obj => { return obj.id === layerID }); //assign the layer that got passed in via id
        } else { 
          layerID = cardLayers[0].id;
          cardLayerData = cardLayers[0] //if no ID was passed, make it the first one.
        }
      }
      $('.layer-legends-'+cardID).addClass("d-none");
      $('#layer-legend-'+layerID).removeClass("d-none");
  
      activeLayers[cardID] = cardLayerData;
      mymap.addLayer({
        'id': cardLayerData.id,
        'type': cardLayerData.type,
        'source': {
          'id': cardLayerData.id+'-source',
          'type': cardLayerData.type,
          'tiles': [cardLayerData.url + layerArgs],
          'tileSize': 256,
          'scheme': cardLayerData.scheme,
        },
        'paint': {}
      }, 'countrySelected');
      if (cardLayerData.type == "raster"){
        var sliderID = document.querySelector('[id^="layerSlider-'+layerID+'"]').id;
        var slider = document.getElementById(sliderID);
        slider.addEventListener('input', function (e) {
        mymap.setPaintProperty(
          cardLayerData.id,
          'raster-opacity',
          parseInt(e.target.value, 10) / 100
          );
        });
      }
    }
    $.fn.removeCardMapLayer = function(cardID){
      if (activeLayers.hasOwnProperty(cardID)){
        mymap.removeLayer(activeLayers[cardID].id);
        mymap.removeSource(activeLayers[cardID].id);
        delete activeLayers[cardID]; 
      }	
    }
    $.fn.removeCardButtonMapLayer = function(LayerID){
      if (activeLayers.hasOwnProperty(LayerID)){
        mymap.removeLayer(activeLayers[LayerID].id);
        mymap.removeSource(activeLayers[LayerID].id);
        delete activeLayers[LayerID]; 
      }	
    }

    function checkMapLayerZoomPadding(){
      var zoomOption = null;
      var currentPath = window.location.pathname;
      if(currentPath.includes("conservation_tracking")){
        zoomOption = "ct"
      }
      var zoomOptions = {
        padding: {top: 10, bottom:10, left: 10, right: 10}
      };
      switch(zoomOption){
        case "ct":
          zoomOptions = {
            padding: {top: 10, bottom:10, left: 10, right: 300}
          };
          break;
        default:	//the country charts are the default for now....
          break;
      }
      return zoomOptions;
    }

    $.fn.moveMapLayers = function(nodeID, direction){
      //$().moveMapLayers(nodeID, direction); 
      const layers = thisMap.getStyle().layers;
      //console.log(layers);
      let firstSymbolId;
      for (const layer of layers) {
          if (layer.type === 'symbol') {
              firstSymbolId = layer.id;
              break;
          }
      }

      $("#id-" + nodeID + " .field--name-field-indi-map-layers-all .field__items" ).children().each(function () {
          var tabLayerKey = '-b10p4m4'+"#id-" + nodeID;
          var mapLayer = $(this).find(".map-layer").text();
          mapLayer = JSON.parse(mapLayer.replace("'", "\""));
          var LayerID = mapLayer.id + tabLayerKey;
          var checkMapLayer = thisMap.getLayer(LayerID);
          if(typeof checkMapLayer !== 'undefined') {
              if (direction == "up"){
                  thisMap.moveLayer(LayerID);
              } else if (direction == "down") {
                thisMap.moveLayer(LayerID, firstSymbolId);
              } else {
                  var customLayer = thisMap.getLayer(direction);
                  if(typeof customLayer !== 'undefined') {
                    thisMap.moveLayer(LayerID, direction);
                  }
              }   
          }
      });
    }

    $.fn.setLayerOpactity = function(map, layer, opacity){
      if (map.getLayer(layer)) {
          map.setPaintProperty(layer, 'raster-opacity', opacity);
      }
  }
})(jQuery);
